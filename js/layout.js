var Layout = (function(name){
	var Auth = {
		controls: {
			login: $(".jLogin"),
			password: $(".jPassword"),
			send: $(".jSend")
		},
		addUser: function(){

		},
		confirmData: function(){
			this.controls.login.removeClass("error");
			this.controls.password.removeClass("error");
			var isCorrect = true;
			if (this.controls.login === "") {
				this.controls.login.addClass("error");
				isCorrect = false;
			}
			if (this.controls.password === "") {
				this.controls.password.addClass("error");
				isCorrect = false;
			}
			return isCorrect;
		},
		run: function() {
			this.bindEvents();
		},
		checkEnabling: function() {
			if (this.controls.login.val() !== "" && this.controls.password.val() !== "") {
					this.controls.send.removeAttr("disabled");
				}
				else {
					this.controls.send.attr("disabled", "disabled");
				}
		},
		bindEvents: function() {
			var that = this;
			this.controls.send.on("click", function(){
				if (that.confirmData()) {
					that.controls.send.removeAttr("disabled");
				}
			});
			this.controls.login.on("keyup", function(){
				that.checkEnabling();
			});
			this.controls.password.on("keyup", function(){
				that.checkEnabling();
			});
		}
	};

	function User(name, password){
		name = name;
		password = password;
	}
	alert(name);

	Auth.run();
})("Evgeniya");